class SubscriptionsController < ApplicationController
  def create
    @subscription = Subscription.new(subscription_params)
    @subscription.save
    if @subscription.save!
      redirect_to :root, notice: "Successfully subscribed!"
    else
      redirect_to :root, error: "Error occured while subscribing! Write directly to nalcarya1881@gmail.com"
    end
  end

  private

  def subscription_params
    params.require(:subscription).permit(:email, :country)
  end
end
