# frozen_string_literal: true

class StaticPagesController < ApplicationController
  before_action :set_event, only: [:show]

  def index
    @upcoming = Event.where('event_date_time >= ?', Date.current).order(:event_date_time).first || Event.where(default: true).first
    @crew = Musician.where(main_page: true).order(created_at: :asc)
  end
end
