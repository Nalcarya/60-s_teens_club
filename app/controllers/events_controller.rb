class EventsController < ApplicationController
  before_action :set_event, only: [:show]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    @upcoming_events = Event.where('event_date_time > ?', DateTime.current)
    @upcoming = @upcoming_events.order(event_date_time: :asc).first
    @subscription = Subscription.new
  end

  # GET /events/1
  # GET /events/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def event_params
    params.require(:event).permit(:published, :event_date_time, :title_en, :title_ru)
  end
end
