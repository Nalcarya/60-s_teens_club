class Musician < ApplicationRecord
  belongs_to :admin, required: false
  has_one_attached :main_image

  validates :name_en, :name_ru, presence: true
  validates :name_ru, presence: true
  validates :full_name_en, presence: true
  validates :full_name_ru, presence: true
end
