class Event < ApplicationRecord
  has_many_attached :images
  has_one_attached :poster
  has_rich_text :text_en
  has_rich_text :text_ru

  has_many :videos
end
