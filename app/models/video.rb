class Video < ApplicationRecord
  belongs_to :event, required: false
  has_one_attached :videofile
end
