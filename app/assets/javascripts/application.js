// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require_tree .
//= require html5shiv
//= require jquery.min
//= require parallax.min

function detectMsBrowser() {
    using_ms_browser =
      navigator.appName == "Microsoft Internet Explorer" ||
      (navigator.appName == "Netscape" &&
        navigator.appVersion.indexOf("Edge") > -1) ||
      (navigator.appName == "Netscape" &&
        navigator.appVersion.indexOf("Trident") > -1);

    if (using_ms_browser == true) {
      alert(
        "Please use Chrome or Firefox for the best browsing experience!"
      );
    }
  }
  function setBrandMarginTop() {
    var bottomContainerHeight = $(".tm-welcome-container").height();

    $(".tm-brand-container-outer").css({
      "margin-top": -bottomContainerHeight + "px"
    });
  }

  $(function() {
    setBrandMarginTop();
    detectMsBrowser();

    // Handle window resize event
    $(window).resize(function() {
      setBrandMarginTop();
    });
  });