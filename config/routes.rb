Rails.application.routes.draw do
  resources :videos
  resources :events
  resources :subscriptions
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # root 'events#index'
  root 'static_pages#index'

  get 'available_sailings', to: 'sailings#index'
end
