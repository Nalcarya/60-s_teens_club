# README

* Ruby version 2.6.3 (see .ruby-version file)

* 

'rvm use 2.6.3@60steensclub --create'
(--create только в первый раз)

* rake db:create

* rake db:migrate

* rake db:seed

* you can login as admin at '/admin',
  u.email = 'qwer@qwer.com'
  u.password = 'qwerty123'
  (this info is also in seeds.rb file (db/seeds.rb))

# export NODE_OPTIONS=--openssl-legacy-provider