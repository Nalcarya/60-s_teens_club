require 'rails_helper'

RSpec.describe 'events/index', type: :view do
  before(:each) do
    assign(:events, [
             Event.create!(
               published: false,
               lat: '9.99',
               lng: '9.99',
               title_en: 'Title En',
               title_ru: 'Title Ru',
               text_en: 'MyText',
               text_ru: 'MyText'
             ),
             Event.create!(
               published: false,
               lat: '9.99',
               lng: '9.99',
               title_en: 'Title En',
               title_ru: 'Title Ru',
               text_en: 'MyText',
               text_ru: 'MyText'
             )
           ])
  end

  it 'renders a list of events' do
    render
    assert_select 'tr>td', text: false.to_s, count: 2
    assert_select 'tr>td', text: '9.99'.to_s, count: 2
    assert_select 'tr>td', text: '9.99'.to_s, count: 2
    assert_select 'tr>td', text: 'Title En'.to_s, count: 2
    assert_select 'tr>td', text: 'Title Ru'.to_s, count: 2
    assert_select 'tr>td', text: 'MyText'.to_s, count: 2
    assert_select 'tr>td', text: 'MyText'.to_s, count: 2
  end
end
