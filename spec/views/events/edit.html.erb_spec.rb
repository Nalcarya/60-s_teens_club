require 'rails_helper'

RSpec.describe 'events/edit', type: :view do
  before(:each) do
    @event = assign(:event, Event.create!(
                              published: false,
                              lat: '9.99',
                              lng: '9.99',
                              title_en: 'MyString',
                              title_ru: 'MyString',
                              text_en: 'MyText',
                              text_ru: 'MyText'
                            ))
  end

  it 'renders the edit event form' do
    render

    assert_select 'form[action=?][method=?]', event_path(@event), 'post' do
      assert_select 'input[name=?]', 'event[published]'

      assert_select 'input[name=?]', 'event[lat]'

      assert_select 'input[name=?]', 'event[lng]'

      assert_select 'input[name=?]', 'event[title_en]'

      assert_select 'input[name=?]', 'event[title_ru]'

      assert_select 'textarea[name=?]', 'event[text_en]'

      assert_select 'textarea[name=?]', 'event[text_ru]'
    end
  end
end
