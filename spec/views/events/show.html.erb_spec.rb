require 'rails_helper'

RSpec.describe 'events/show', type: :view do
  before(:each) do
    @event = assign(:event, Event.create!(
                              published: false,
                              lat: '9.99',
                              lng: '9.99',
                              title_en: 'Title En',
                              title_ru: 'Title Ru',
                              text_en: 'MyText',
                              text_ru: 'MyText'
                            ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/false/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Title En/)
    expect(rendered).to match(/Title Ru/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
  end
end
