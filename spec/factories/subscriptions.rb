FactoryBot.define do
  factory :subscription do
    country { 'MyString' }
    email { 'MyString' }
    notify { false }
  end
end
