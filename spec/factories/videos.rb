FactoryBot.define do
  factory :video do
    title { 'MyString' }
    youtube_link { 'MyText' }
  end
end
