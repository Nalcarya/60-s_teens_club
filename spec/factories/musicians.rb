FactoryBot.define do
  factory :musician do
    name_en { 'MyString' }
    name_ru { 'MyString' }
    full_name_en { 'MyString' }
    full_name_ru { 'MyString' }
    from { '2020-09-27' }
    to { '2020-09-27' }
    bio_en { 'MyString' }
    bio_ru { 'MyString' }
    image { 'MyString' }
    main_page { false }
    admin_id { false }
  end
end
