FactoryBot.define do
  factory :event do
    published { false }
    event_date_time { '2020-07-20 04:53:13' }
    title_en { 'MyString' }
    title_ru { 'MyString' }
    text_en { 'MyText' }
    text_ru { 'MyText' }
  end
end
