FROM ruby:3.1.2
RUN curl https://deb.nodesource.com/setup_16.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -yqq \
  && apt-get install -yqq --no-install-recommends \
    postgresql-client \
    nodejs \
    yarn \
  && apt-get -q clean \
  && rm -rf /var/lib/apt/lists

RUN mkdir /60steens
WORKDIR /60steens

COPY Gemfile ./
COPY Gemfile.lock ./

RUN gem install bundler
RUN bundle install

COPY . .

RUN yarn install --check-files

EXPOSE 3000
CMD rails server -b 0.0.0.0