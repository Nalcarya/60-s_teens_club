# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_08_08_001800) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "events", force: :cascade do |t|
    t.boolean "published"
    t.text "location"
    t.datetime "event_date_time", precision: nil
    t.string "title_en"
    t.string "title_ru"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "default", default: false, null: false
    t.index ["event_date_time"], name: "index_events_on_event_date_time", unique: true
    t.index ["location"], name: "index_events_on_location"
    t.index ["title_en"], name: "index_events_on_title_en", unique: true
    t.index ["title_ru"], name: "index_events_on_title_ru", unique: true
  end

  create_table "lyrics", force: :cascade do |t|
    t.text "title", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_lyrics_on_title"
  end

  create_table "musicians", force: :cascade do |t|
    t.string "name_en", null: false
    t.string "name_ru", null: false
    t.string "full_name_en", null: false
    t.string "full_name_ru", null: false
    t.date "from"
    t.date "to"
    t.text "bio_en"
    t.text "bio_ru"
    t.boolean "main_page"
    t.bigint "admin_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "instruments", default: "", null: false
    t.index ["admin_id"], name: "index_musicians_on_admin_id"
    t.index ["from"], name: "index_musicians_on_from"
    t.index ["full_name_en"], name: "index_musicians_on_full_name_en"
    t.index ["full_name_ru"], name: "index_musicians_on_full_name_ru"
    t.index ["name_en"], name: "index_musicians_on_name_en"
    t.index ["name_ru"], name: "index_musicians_on_name_ru"
    t.index ["to"], name: "index_musicians_on_to"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string "country"
    t.string "email", null: false
    t.boolean "notify", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "videos", force: :cascade do |t|
    t.string "title_en"
    t.string "title_ru"
    t.text "youtube_link"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_videos_on_event_id"
    t.index ["title_en"], name: "index_videos_on_title_en", unique: true
    t.index ["title_ru"], name: "index_videos_on_title_ru", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
end
