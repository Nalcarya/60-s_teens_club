# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# create test admin user
Admin.create do |u|
  u.email = 'qwer@qwer.com'
  u.password = 'qwerty123'
  u.password_confirmation = 'qwerty123'
end
