class CreateLyrics < ActiveRecord::Migration[6.0]
  def change
    create_table :lyrics do |t|
      t.text :title, null: false
      t.text :description

      t.timestamps
    end

    add_index :lyrics, :title
  end
end
