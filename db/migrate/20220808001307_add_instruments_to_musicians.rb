# frozen_string_literal: true

# adds instruments to musicians
class AddInstrumentsToMusicians < ActiveRecord::Migration[7.0]
  def change
    add_column :musicians, :instruments, :string, null: false, default: ''
  end
end
