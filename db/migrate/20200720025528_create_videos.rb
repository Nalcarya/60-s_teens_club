class CreateVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :videos do |t|
      t.string :title_en
      t.string :title_ru
      t.text :youtube_link
      t.belongs_to :event

      t.timestamps
    end

    add_index :videos, :title_en,                unique: true
    add_index :videos, :title_ru,                unique: true
  end
end
