# frozen_string_literal: true

# default event is shown when there are no upcoming gigs
class AddDefaultToEvent < ActiveRecord::Migration[7.0]
  def change
    add_column :events, :default, :boolean, null: false, default: false
  end
end
