class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.boolean :published
      t.text :location
      t.datetime :event_date_time
      t.string :title_en
      t.string :title_ru

      t.timestamps
    end

    add_index :events, :event_date_time, unique: true
    add_index :events, :title_en,                unique: true
    add_index :events, :title_ru,                unique: true
    add_index :events, :location
  end
end
