class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.string :country
      t.string :email, null: false
      t.boolean :notify, null: false, default: true

      t.timestamps
    end
  end
end
