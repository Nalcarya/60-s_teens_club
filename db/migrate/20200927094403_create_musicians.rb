class CreateMusicians < ActiveRecord::Migration[6.0]
  def up
    create_table :musicians do |t|
      t.string :name_en, null: false
      t.string :name_ru, null: false
      t.string :full_name_en, null: false
      t.string :full_name_ru, null: false
      t.date :from
      t.date :to
      t.text :bio_en
      t.text :bio_ru
      t.boolean :main_page
      t.belongs_to :admin, required: false

      t.timestamps
    end

    add_index :musicians, :name_en
    add_index :musicians, :name_ru
    add_index :musicians, :full_name_en
    add_index :musicians, :full_name_ru
    add_index :musicians, :from
    add_index :musicians, :to
  end

  def down
    drop_table :musicians
  end
end
